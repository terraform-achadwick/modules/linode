#Create linode instance
resource "linode_instance" "instance" {
  label           = "${var.project_name}-instance"
  region          = var.region
  type            = var.linode_type

  disk {
    label = "boot"
    size = var.sda_size
    image  = var.linode_image
    authorized_keys = [chomp(file("${var.key_path}.pub"))] #removes any trailing \n
    root_pass       = var.root_pass
  }

  config {
    label = "boot_config"
    kernel = "linode/latest-64bit"
    devices {
      sda {
        disk_label = "boot"
      }
      sdb {
        volume_id = var.volume_id
      }
    }
    root_device = "/dev/sda"
  }

  boot_config_label = "boot_config"

  tags = [ var.project_name ]
}