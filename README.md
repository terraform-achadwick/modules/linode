Module "Linode"
=========

Used to provision new Linode vps instances


Example 
----------------
>main.tf
```
terraform {
  required_providers {
    linode = {
      source = "linode/linode"
      version = "~> 1.13.3"
    }
  }

provider "linode" {
  token = "<linode-token>"
}

module "linode" {
  source = "git::https://gitlab.com/terraform-achadwick/molules/linode?ref=v0.1.0"
  
  key_path = var.key_path
  linode_image = "linode/centos8"
  linode_type = "g6-standard-1"
  project_name = "dwick-selfhost"
  region = "eu-west"
  root_pass = var.root_pass
}

output "instance_public_ip" {
  value = module.linode.instance_public_ip
}
```